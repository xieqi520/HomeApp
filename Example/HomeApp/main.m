//
//  main.m
//  HomeApp
//
//  Created by xieqi on 08/13/2021.
//  Copyright (c) 2021 xieqi. All rights reserved.
//

@import UIKit;
#import "XQAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([XQAppDelegate class]));
    }
}
