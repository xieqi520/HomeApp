//
//  XQAppDelegate.h
//  HomeApp
//
//  Created by xieqi on 08/13/2021.
//  Copyright (c) 2021 xieqi. All rights reserved.
//

@import UIKit;

@interface XQAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
